var express    = require('express');        
var app        = express();                 
var bodyParser = require('body-parser'); 
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
var port = process.env.PORT || 8080;        
var router = express.Router();
const MongoClient = require('mongodb').MongoClient
const request = require('request');

MongoClient.connect('mongodb://localhost:27017/prueba', (err, client) => {
  	if (err) return console.error(err);
  	console.log('Connected to Database');
	const db = client.db('prueba');
	var json = {};
	request('https://hn.algolia.com/api/v1/search_by_date?query=nodejs', { json: true }, (err, res, body) => {
		if (err) { 
		  return console.log(err); 
		}
		console.log(body);
		db.collection('news').insert(body.hits, function (err, result) {
			console.log(err);
			console.log(result);
		})
	});
})

router.get('/', function(req, res) {
    res.send("Prueba reign");
});

router.get('/news', function (req, res) {
	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	MongoClient.connect('mongodb://localhost:27017/prueba', (err, client) => {
	  	if (err) return console.error(err);
	  	console.log('Connected to Database');
		const db = client.db('prueba');
		db.collection('news').find().toArray()
	    .then(results => {
	      res.send(results);
	    })
	    .catch(error => console.error(error))
	})
});

router.get('/news/:id', function (req, res) {
  	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  	var id = req.params.id;
	MongoClient.connect('mongodb://localhost:27017/prueba', (err, client) => {
	  	if (err) return console.error(err);
	  	console.log('Connected to Database');
		const db = client.db('prueba');
    	db.collection('news').find({_id:id}).toArray()
	    .then(results => {
	      res.send(results);
	    })
	    .catch(error => console.error(error))
	})
});

router.get('/news/delete/:id', function (req, res) {
  	res.header("Access-Control-Allow-Origin", "*");
  	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  	var id = req.params.id;
	MongoClient.connect('mongodb://localhost:27017/prueba', (err, client) => {
	  	if (err) return console.error(err);
	  	console.log('Connected to Database');
		const db = client.db('prueba');
    	/*db.collection('news').deleteOne({_id:id})
	    .then(results => {
	      res.send(results);
	    })*/

	    db.collection("news").deleteOne({_id:id}, function(err, obj) {
		if (err) throw err;
			console.log("1 document deleted");
		});
	    //.catch(error => console.error(error))
	})
});

app.use('/prueba', router);
app.listen(port);

console.log('Aplicación creada en el puerto: ' + port);